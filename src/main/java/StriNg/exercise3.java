package StriNg;

public class exercise3 {
    public static void main(String[] args) {
        System.out.println(lengthOfWordInString("Сидел петух на лавочке считал свои булавочки"));
        String[] arr = {"Хурма","кот"};
        changeLastSymbols(arr,4);
        System.out.println(addSpace("1 3, 343,2"));
        System.out.println(oneEks("куууу віві"));
        System.out.println(countWords("ку івів вівіфффф"));
        System.out.println(delete("1232123",3,5));
        System.out.println(reverseString("12345"));
        System.out.println(deleteLastWord("2312 123 4"));
    }

    public static int lengthOfWordInString(String ar) {

        String results = ar.replace(",", "");
        String[] arr = results.split(" ");

        int count = arr[0].length();

        for (int i = 0; i < arr.length; i++) {

            if (count > arr[i].length()) {

                count = arr[i].length();
            }
        }
        return count;
    }
    public static String[] changeLastSymbols(String[] arr, int length) {

        for (int i = 0; i < arr.length; i++) {
            if (arr[i].length()>=length){
                String temp = arr[i];
                char[] arrayForString = temp.toCharArray();
                arrayForString[arrayForString.length - 1] = '$';
                arrayForString[arrayForString.length - 2] = '$';
                arrayForString[arrayForString.length - 3] = '$';
                arr[i] = String.copyValueOf(arrayForString);


            }
            System.out.print(arr[i]+" ");
        }

        return arr;
    }

    public static String oneEks(String str){
        char[]chars = str.toCharArray();
        StringBuilder sb = new StringBuilder();
        boolean repeatedChar;
        for (int i = 0; i < chars.length; i++) {
            repeatedChar = false;
            for (int j = i + 1; j < chars.length; j++) {
                if (chars[i]== chars[j]) {
                    repeatedChar = true;
                    break;
                }
            }
            if (!repeatedChar) {
                sb.append(chars[i]);
            }
        }
        return sb.toString();
    }



    public static String addSpace(String x) {

        return x.replace(",", ", ").replace("  ", " ");
    }
    public static int countWords(String str){
        int count = 0;
        if(str.length() != 0){
            count++;
            for (int i = 0; i < str.length(); i++) {
                if(str.charAt(i) == ' '){
                    count++;
                }
            }
        }
        return count;
    }
    public static String delete(String ar, int from, int to) {

        char[] arr = ar.toCharArray();
        for (int i = from; i < to; i++) {
            arr[i] = ' ';
        }
        String q = String.copyValueOf(arr);

        return q.replace(" ", "");
    }
    public static String reverseString(String str){
        char[] array = str.toCharArray();
        String result = "";
        for (int i = array.length - 1; i >= 0; i--) {
            result = result + array[i];
        }
        return result;
    }
    public static String deleteLastWord(String x) {
        String[] arr = x.split(" ");
        arr[arr.length - 1] = "";
        StringBuffer lastResult = new StringBuffer();

        for (int i = 0; i < arr.length; i++) {
            lastResult.append(arr[i] + " ");
        }
        return lastResult.toString();

    }

}
