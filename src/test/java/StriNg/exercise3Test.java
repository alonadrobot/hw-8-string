package StriNg;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

class exercise3Test {
    exercise3 cut = new exercise3();

    static Arguments[] strTestArgs(){
        return new Arguments[]{
                Arguments.arguments("Машина Картошка Водитель",6),
                Arguments.arguments("Харьков Киев Запорожье",4)
        };
    }
    @ParameterizedTest
    @MethodSource("strTestArgs")
    void strTest(String ar,int expected){
        int actual=cut.lengthOfWordInString(ar);
        assertEquals(expected,actual);
    }



    static Arguments[] changeLastSymbolsTestArgs(){
        String[] arr1={"Gradiant","House"};
        String[] arr2={"Hello","Durable","Since"};
        String[] arrTest1={"Gradi$$$","House"};
        String[] arrTest2={"He$$$","Dura$$$","Si$$$"};
        return new Arguments[]{
                Arguments.arguments(arr1,6,arrTest1),
                Arguments.arguments(arr2,5,arrTest2)
        };
    }
    @ParameterizedTest
    @MethodSource("changeLastSymbolsTestArgs")
    void changeLastSymbolsTest(String[] arr,int length,String[] expected){
        String[] actual=cut.changeLastSymbols(arr,length);
        Assertions.assertArrayEquals(expected,actual);
    }


    static Arguments[] addSpaceTestArgs(){
        return new Arguments[]{
                Arguments.arguments("1 3,343,2","1 3, 343, 2"),
                Arguments.arguments("14,3,5, 6, 7","14, 3, 5, 6, 7")
        };
    }
    @ParameterizedTest
    @MethodSource("addSpaceTestArgs")
    void addSpaceTest(String str,String expected){
        String  actual=cut.addSpace(str);
        assertEquals(expected,actual);
    }


    static Arguments[] oneEksTestArgs(){
        return new Arguments[]{
                Arguments.arguments("1 3,343,2","1 43,2"),
                Arguments.arguments("14,3,5, 6, 7","14356, 7")
        };
    }
    @ParameterizedTest
    @MethodSource("oneEksTestArgs")
    void oneEksTest(String str,String expected){
        String  actual=cut.oneEks(str);
        assertEquals(expected,actual);
    }



    static Arguments[] deleteTestArgs(){
        return new Arguments[]{
                Arguments.arguments("1232123",3,5,"12323"),
                Arguments.arguments("12321",3,4,"1231")
        };
    }
    @ParameterizedTest
    @MethodSource("deleteTestArgs")
    void deleteTest(String ar, int from, int to,String expected){
        String  actual=cut.delete(ar,from,to);
        assertEquals(expected,actual);
    }



    static Arguments[] reverseTestArgs(){
        return new Arguments[]{
                Arguments.arguments("123","321"),
                Arguments.arguments("12345","54321")
        };
    }
    @ParameterizedTest
    @MethodSource("reverseTestArgs")
    void reverseTest(String str,String expected){
        String  actual=cut.reverseString(str);
        assertEquals(expected,actual);
    }


    static Arguments[] countTestArgs(){
        return new Arguments[]{
                Arguments.arguments("hi hello world",3),
                Arguments.arguments("12 23,24",2)
        };
    }
    @ParameterizedTest
    @MethodSource("countTestArgs")
    void countTest(String str,int expected){
        int actual=cut.countWords(str);
        assertEquals(expected,actual);
    }

}