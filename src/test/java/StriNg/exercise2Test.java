package StriNg;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

class exercise2Test {
    exercise2 cut = new exercise2();

    static Arguments[] strTestArgs(){
        return new Arguments[]{
                Arguments.arguments("27",27),
                Arguments.arguments("1234",1234)
        };
    }
    @ParameterizedTest
    @MethodSource("strTestArgs")
    void strTest(String str,int expected){
        int actual=cut.stringInInteger(str);
        assertEquals(expected,actual);
    }
    static Arguments[] str2TestArgs(){
        return new Arguments[]{
                Arguments.arguments(27,"27"),
                Arguments.arguments(1234,"1234")
        };
    }
    @ParameterizedTest
    @MethodSource("str2TestArgs")
    void str2Test(int number,String expected){
        String actual=cut.integerInString(number);
        assertEquals(expected,actual);
    }

    static Arguments[] str3TestArgs(){
        return new Arguments[]{
                Arguments.arguments(27.555,"27.555"),
                Arguments.arguments(1234.0,"1234.0")
        };
    }
    @ParameterizedTest
    @MethodSource("str3TestArgs")
    void str3Test(double number,String expected){
        String actual=cut.doubleInString(number);
        assertEquals(expected,actual);
    }


    static Arguments[] str4TestArgs(){
        return new Arguments[]{
                Arguments.arguments("27.042",27.042),
                Arguments.arguments("1234.1234",1234.1234)
        };
    }
    @ParameterizedTest
    @MethodSource("str4TestArgs")
    void str4Test(String str,double expected){
        double actual=cut.stringInDouble(str);
        assertEquals(expected,actual);
    }

}